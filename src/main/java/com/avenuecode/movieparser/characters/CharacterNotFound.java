package com.avenuecode.movieparser.characters;

@SuppressWarnings("serial")
public class CharacterNotFound extends RuntimeException{
	private long id;
	public CharacterNotFound(long id) {
		this.setId(id);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
