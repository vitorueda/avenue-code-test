package com.avenuecode.movieparser.characters;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.avenuecode.movieparser.settings.Settings;
import com.avenuecode.movieparser.words.Words;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Characters {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String name;

	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "characters", cascade = CascadeType.ALL)
	@JsonBackReference
	private Set<Settings> settings;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "character", cascade = CascadeType.ALL)
	@JsonManagedReference
	@OrderBy("counter DESC")
	@JsonIgnoreProperties({"id"})
	@Fetch (FetchMode.SELECT)
	private List<Words> wordCounts;

	protected Characters() {
	}

	public Characters(String name) {
		this.setSettings(new LinkedHashSet<Settings>());
		this.setName(name);
		this.setWordCounts(new ArrayList<Words>());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Settings> getSettings() {
		return settings;
	}

	public void setSettings(Set<Settings> settings) {
		this.settings = settings;
	}

	public List<Words> getWordCounts() {
		if(wordCounts.size()> 10) {
			return wordCounts.subList(0, 10);
		}
		return wordCounts;
	}

	public void setWordCounts(List<Words> wordCounts) {
		this.wordCounts = wordCounts;
	}

	public void said(String line) {
		line = line.replaceAll("[.,!?\";]", "");
		String words[] = line.split("\\s");
		for (String word : words) {
			wordCount(word);
		}
	}

	private void wordCount(String word) {
		word = word.toLowerCase();
		for(Words w: this.wordCounts) {
			if(word.equals(w.getWord())) {
				w.setCounter(w.getCounter()+1);
				return;
			}
		}
		Words w = new Words(word);
		w.setCharacter(this);
		this.wordCounts.add(w);
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Characters))
			return false;
		if (obj == this)
			return true;

		Characters c = (Characters) obj;
		return (getName().equals(c.getName()));
	}
}
