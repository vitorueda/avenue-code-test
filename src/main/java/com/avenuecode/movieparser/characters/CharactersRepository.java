package com.avenuecode.movieparser.characters;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface CharactersRepository extends CrudRepository<Characters, Long> {
	@Override
	List<Characters> findAll();
}
