package com.avenuecode.movieparser.words;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.avenuecode.movieparser.settings.Settings;

public interface WordsRepository  extends PagingAndSortingRepository<Words, Long> {
	List<Settings> findByWord(String name);

	@Override
	List<Words> findAll();
	
}
