package com.avenuecode.movieparser.words;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.avenuecode.movieparser.characters.Characters;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Words {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String word;

	private int counter;

	@ManyToOne
	@JsonBackReference
	private Characters character;

	protected Words() {
	}

	public Words(String word) {
		this.word = word;
		this.counter = 1;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public Characters getCharacter() {
		return character;
	}

	public void setCharacter(Characters character) {
		this.character = character;
	}

	@Override
	public int hashCode() {
		return getWord().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Words))
			return false;
		if (obj == this)
			return true;

		Words word = (Words) obj;
		return (getWord().equals(word.getWord()));
	}
	@Override
	public String toString() {
		return this.getWord() + " " + this.getId();
	}

}
