package com.avenuecode.movieparser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.movieparser.characters.CharacterNotFound;
import com.avenuecode.movieparser.characters.Characters;
import com.avenuecode.movieparser.characters.CharactersRepository;
import com.avenuecode.movieparser.script.Script;
import com.avenuecode.movieparser.settings.Settings;
import com.avenuecode.movieparser.settings.SettingsRepository;
import com.avenuecode.movieparser.words.WordsRepository;

@RestController
public class Controller {

	@Autowired
	SettingsRepository sRepo;

	@Autowired
	CharactersRepository cRepo;

	@Autowired
	WordsRepository wRepo;

	@RequestMapping(value = "/script", method = RequestMethod.POST, consumes = "text/plain; charset=utf-8", produces = "application/json")
	public ResponseEntity<?> postScript(@RequestBody String text) {
		if (sRepo.findAll().size() > 0) {
			Message msg = new Message("Movie script already received");
			return new ResponseEntity<>(msg, HttpStatus.FORBIDDEN);
		}
		Script script = new Script(text);
		for (Settings s : script.getSettings().values()) {
			sRepo.save(s);
		}
		Message msg = new Message("Movie script successfully received");
		return new ResponseEntity<>(msg, HttpStatus.OK);
	}

	@RequestMapping(value = "/settings")
	public List<Settings> allSettings() {
		List<Settings> settings = sRepo.findAll();
		return settings;

	}

	@RequestMapping(value = "/settings/{id}", produces = "application/json")
	public Settings getSettings(@PathVariable("id") long id) {
		Settings set = sRepo.findOne(id);
		if (set == null) {
			throw new CharacterNotFound(id);
		}
		return set;
	}

	@RequestMapping(value = "/characters")
	public List<Characters> allCharacters() {
		List<Characters> cs = cRepo.findAll();
		return cs;
	}

	@RequestMapping(value = "/characters/{id}", produces = "application/json")
	public Characters getCharacters(@PathVariable("id") long id) {
		Characters c = cRepo.findOne(id);
		if (c == null) {
			throw new CharacterNotFound(id);
		}
		return c;
	}
}
