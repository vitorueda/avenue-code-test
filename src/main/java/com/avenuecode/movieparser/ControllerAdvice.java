package com.avenuecode.movieparser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.avenuecode.movieparser.characters.CharacterNotFound;
import com.avenuecode.movieparser.settings.SettingNotFound;

@ControllerAdvice
class GlobalControllerExceptionHandler {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CharacterNotFound.class)
    @ResponseBody
    public ResponseEntity<?> characterNotFoundException(CharacterNotFound ex) {
    	Message msg = new Message("Movie character with id " + ex.getId() + " not found");
		return new ResponseEntity<>(msg, HttpStatus.NOT_FOUND);
    }
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(SettingNotFound.class)
    @ResponseBody
    public ResponseEntity<?> settingNotFoundException(SettingNotFound ex) {
    	Message msg = new Message("Movie setting with id " + ex.getId() + " not found");
		return new ResponseEntity<>(msg, HttpStatus.NOT_FOUND);
    }
    
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<?> defaultError() {
    	Message msg = new Message("Unexpected error");
		return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
