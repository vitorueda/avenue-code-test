package com.avenuecode.movieparser.script;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.avenuecode.movieparser.settings.Settings;

public class Script {
	private Map<String, Settings> settings;

	public Script(String text) {
		setSettings(new LinkedHashMap<String, Settings>());
		splitSettings(text);
	}


	public Map<String, Settings> getSettings() {
		return settings;
	}


	public void setSettings(Map<String, Settings> settings) {
		this.settings = settings;
	}


	/**
	 * Split the Script text received to strings that cointains
	 * just a setting. 
	 *
	 * @param  script movie script
	 */
	private Settings getSetting(String name) {
		Settings s = getSettings().get(name);
		if(s == null) {
			s = new Settings(name);
			getSettings().put(name, s);
		}
		return s;
	}

	/**
	 * Split the Script text received to strings that cointains
	 * just a setting. 
	 *
	 * @param  script movie script
	 */
	private void splitSettings(String script) {
		Scanner sc = new Scanner(script);
		Pattern pattern = Pattern.compile("(EXT\\.|INT\\.|INT\\.\\/EXT\\.) ");
		sc.useDelimiter(pattern);
		// Remove opening crawl
		sc.next();
		while (sc.hasNext()) {
			settingParser(sc.next());
		}
		sc.close();
	}

	private void settingParser(String text) {
		Scanner scanner = new Scanner(text);
		String name = scanner.nextLine().replaceAll(" - (.+)", "");
		// Considering NAME. and NAME equals
		name = name.replaceAll("\\.", "");
		scanner.close();
		Settings set = getSetting(name);
		set.parser(text);
	}
}
