package com.avenuecode.movieparser.settings;

@SuppressWarnings("serial")
public class SettingNotFound extends RuntimeException{
	private long id;
	public SettingNotFound(long id) {
		this.setId(id);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}