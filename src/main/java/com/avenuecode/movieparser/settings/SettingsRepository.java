package com.avenuecode.movieparser.settings;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface SettingsRepository extends CrudRepository<Settings, Long> {
	List<Settings> findByName(String name);

	@Override
	List<Settings> findAll();

}
