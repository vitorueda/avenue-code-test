package com.avenuecode.movieparser.settings;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import com.avenuecode.movieparser.characters.Characters;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Settings {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(unique = true)
	private String name;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "settings_characters", joinColumns = @JoinColumn(name = "character_id") , inverseJoinColumns = @JoinColumn(name = "setting_id") )
	@JsonManagedReference
	@OrderBy("id ASC")
	private Map<String, Characters> characters;

	@Transient
	private static final Map<String, Characters> allCharachters = new HashMap<String, Characters>();

	protected Settings() {
	}

	public Settings(String name) {
		this.setCharacters(new LinkedHashMap<String, Characters>());
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, Characters> getCharacters() {
		return characters;
	}

	public void setCharacters(Map<String, Characters> characters) {
		this.characters = characters;
	}

	private Characters getCharacter(String name) {
		Characters c = allCharachters.get(name);
		if (c == null) {
			c = new Characters(name);
			allCharachters.put(name, c);
		}
		this.getCharacters().put(name, c);
		return c;
	}

	public void parser(String text) {
		Scanner scanner = new Scanner(text);
		boolean isDialog = false;
		String role = null;
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if (isDialog) {
				if (line.matches(" {10}[^ ](.+)")) {
					String dialog = line.substring(10);
					this.getCharacter(role).said(dialog);
				} else {
					isDialog = false;
				}
			} else if (line.matches(" {22}[A-Z]([A-Z0-9]|'| )*")) {
				role = line.substring(22);
				role = role.replace("\'S VOICE", "");
				isDialog = true;
			}
		}
		scanner.close();
	}

	@Override
	public int hashCode() {
		return getName().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Settings))
			return false;
		if (obj == this)
			return true;

		Settings set = (Settings) obj;
		return (getName().equals(set.getName()));
	}
}
