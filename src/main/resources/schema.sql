DROP TABLE settings_characters IF EXISTS;
DROP TABLE settings IF EXISTS;
DROP TABLE characters IF EXISTS;
DROP TABLE words IF EXISTS;


CREATE TABLE settings (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(255)
);
CREATE INDEX settings_name ON settings (name);

CREATE TABLE characters (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(255)
);
CREATE INDEX characters_name ON characters (name);

CREATE TABLE settings_characters (
  setting_id       INTEGER NOT NULL,
  character_id INTEGER NOT NULL
);
ALTER TABLE settings_characters ADD CONSTRAINT fk_settings_characters_settings FOREIGN KEY (setting_id) REFERENCES settings (id);
ALTER TABLE settings_characters ADD CONSTRAINT fk_settings_characters_characters FOREIGN KEY (character_id) REFERENCES characters (id);

CREATE TABLE words (
  id   INTEGER IDENTITY PRIMARY KEY,
  word VARCHAR(80),
  counter INTEGER NOT NULL,
  character_id  INTEGER NOT NULL
);
CREATE INDEX words_character_id ON words (character_id);

ALTER TABLE words ADD CONSTRAINT fk_words_character FOREIGN KEY (character_id) REFERENCES characters (id);
